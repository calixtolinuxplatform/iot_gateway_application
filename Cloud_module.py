
####**************************************  Import  *****************************************************### 
import datetime
import time
import requests
import os
from subprocess import call
import json

####  File inclusion  ### 
from common import *
from Generator_module import *
from Watertank_module import *
from Freezer_module import *
from Escalator_module import *



### ****************************************** Variable declaration ************************************* ###
TOKEN = ['eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWEPS--3zRlROxWJd3ePgE']
headers             =      [{'Content-type': 'application/json', 'Authorization': 'token {}'.format(TOKEN),'Accept': 'text/plain'}] 
System_reset_count  =      [0]

Gateway_sign_in_packet =   "{\"emailId\":\"iot@user.com\",\"password\":\"Iot@123\",\"gid\":\"355026073360888\"}"
Gateway_sign_in_url    =   " http://52.172.47.241:51203/gateways/signin/"


###***************************************  Function prototype  ******************************************###

#*******************************************************************************
  # @brief  GPRS Enable Pin Initialization.
  # @param  None
  # @retval None
  #****************************************************************************#

def GPRS_enable_pin_init():   
  try:
    GPRS_EN_export=open("/sys/class/gpio/export", "w")
    GPRS_EN_export.write("%d" % (111))
    GPRS_EN_export.close()

    GPRS_EN_direction=open("/sys/class/gpio/gpio111/direction", "w")
    GPRS_EN_direction.write("out")
    GPRS_EN_direction.close()
  except:
    print("Error")

#************************** End of GPRS_enable_pin_init *******************************#


#*******************************************************************************
  # @brief  Deinitialize the GPRS Enable Pin.
  # @param  None
  # @retval None
  #****************************************************************************#
def GPRS_enable_pin_deinit():
  try:
   GPRS_EN_unexport = open("/sys/class/gpio/unexport", "w")
   GPRS_EN_unexport.write("%d" % (111))
   GPRS_EN_unexport.close()
  except:
   print("Error")

#************************** End of GPRS_enable_pin_deinit *******************************#


#*******************************************************************************
  # @brief  Cloud Server signin for security concern.
  # @param  None
  # @retval None
  #****************************************************************************#
def cloud_signin_process():

  cloud_server_response = requests.post(Gateway_sign_in_url, data=Gateway_sign_in_packet, headers=headers[0],timeout=10) 
  
  if DEBUG_PRINTS == DEBUG_ENABLE:
    print(cloud_server_response.text) 
  
  signin_token_packet_string =  cloud_server_response.text
  signin_token_packet_json   =  json.loads(signin_token_packet_string)
  TOKEN[0]                   =  signin_token_packet_json["data"]["token"]
  headers[0]                 =  {'Content-type': 'application/json', 'Authorization': 'Bearer {}'.format(TOKEN[0]),'Accept': 'text/plain'}
            


 
#************************** End of cloud_signin_process *******************************#



#*******************************************************************************
  # @brief  Cloud communication module.
  # @param  thread name
  # @retval None
  #****************************************************************************#
def Cloud_communication_thread(threadname):
    while True:
        
        now = datetime.datetime.now()
        System_Date = str('{0:02d}'.format(now.day))+str('{0:02d}'.format(now.month))+str('{0:04d}'.format(now.year))
        System_Time = str('{0:02d}'.format(now.hour))+str('{0:02d}'.format(now.minute))+str('{0:02d}'.format(now.second))

        gateway_info="{\"GTW\":{\"GID\":\""+Gateway_id+"\",\"LAT\":\"77.4589\",\"LON\":\"13.5896\",\"ALT\":\"0051.000\",\"ACR\":\"000\",\"VEL\":\"0.00\",\"CID\":\"268435455\",\"TIM\":\"" + System_Time +"\",\"DAT\":\"" + System_Date + "\"},\"MTP\":\"01\"," 
        
        if int(Slave_device_table[1][1]) == SLAVE_TYPE_GENERATOR:
          cloud_tx_packet = gateway_info + generator1.Get_gen_parameter_packet()

        elif int(Slave_device_table[1][1]) == SLAVE_TYPE_WATERTANK:
          cloud_tx_packet = gateway_info + watertank1.Get_wtrtnk_parameter_packet()

        elif int(Slave_device_table[1][1]) == SLAVE_TYPE_FREEZER:
          cloud_tx_packet = gateway_info + freezer1.Get_frzr_parameter_packet()

        elif int(Slave_device_table[1][1]) == SLAVE_TYPE_ESCALATOR:
          cloud_tx_packet = gateway_info + escalator1.Get_escalator_parameter_packet()

        else:
          printf("Cloud Module> Invalid slave type")

        print(cloud_tx_packet,"\n\r")

        for i in range (cloud_push_retry):
          try:
            r = requests.post(server_url, data=cloud_tx_packet, headers=headers[0],timeout=10) 
            print(r.text) 
            print(r.status_code)
            if r.status_code == 401:
              cloud_signin_process()
              print("Cloud Module> Sign in required")
            System_reset_count[0] = 0 
            break;
          except:
            print("\nCloud Module> Send Error, Try:",i)
            System_reset_count[0] = System_reset_count[0]+1
            time.sleep(1)

        if System_reset_count[0] >= 9:
          print("\nCloud Module> Network error.......................")
          if  APPLICATION_TARGET  ==  DEVICE_TYPE_GATEWAY:
              GPRS_enable_pin_init()
              print("\nPower off GSM Modem")
              led_2=open("/sys/class/gpio/gpio111/value", "w")
              led_2.write("0")
              time.sleep(5)
              led_2=open("/sys/class/gpio/gpio111/value", "w")
              led_2.write("1")
              print("\nResetting system")
              call("sudo nohup reboot -h now", shell=True)

        time.sleep(cloud_push_interval)

#************************** End of Cloud_communication_thread *******************************#



