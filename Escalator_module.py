
####**************************************  Import  *****************************************************### 
import datetime
import time
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadInputRegistersResponse
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.compat import iteritems
from common import *


### ****************************************** Constant declaration ************************************* ###



### ****************************************** Variable declaration ************************************* ###
ESCLTR_register_values =   [0x00,0x00,0x00]

escltr_read_interval   =   [10]


### ****************************************** Class declaration ************************************* ###
class escalator_parameters:
    def __init__(self,Manufacturer,Parameter_count):
        self.Manufacturer = Manufacturer
        self.Parameter_count = Parameter_count
    
        self.RUN  = '1' 
        self.STP  = '0'
        self.FLT  = '0'
        self.STA  = '1'
        self.DID  = '04'
        self.DTP  = '04'

    def Get_escalator_parameter_packet(self):
        escalator_parameter_json_packet = "\"DEV\":{\"DID\":\""+self.DID+"\",\"DTP\":\""+self.DTP+"\",\"STA\":\""+self.STA+"\",\"RUN\":\""+self.RUN+"\",\"STP\":\""+self.STP+"\",\"FLT\":\""+self.FLT+"\"}}"
        return escalator_parameter_json_packet

escalator1=escalator_parameters("CALIXTO",3)
escalator1.DID = Slave_device_table[1][0]

###***************************************  Function prototype  *************************************************###


# #*******************************************************************************
#   # @brief  Escalator data processing.
#   # @param  Modbu_Slave_ID  - Slave address of the Escalator device
#             Escalator_number - Escalator class object
#   # @retval None
#   #****************************************************************************#
def Esclator_read_modbus_registers(Modbus_Slave_ID, Escalator_number):
        try:
            client = ModbusClient(method = 'rtu',port = modbus_com,stopbits=2,parity='N',baudrate=19200,timeout=5)
            connection = client.connect()
            print(connection)

            Holding_register_value = client.read_holding_registers(0,3,unit=Modbus_Slave_ID)   # Read Generator running Hours
            client.close()

            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Holding_register_value.registers)

            Escalator_number.RUN = str(Holding_register_value.registers[0])
            Escalator_number.STP = str(Holding_register_value.registers[1])
            Escalator_number.FLT = str(Holding_register_value.registers[2])

            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Escalator_number.RUN,Escalator_number.STP,Escalator_number.FLT)
            
        except:
            Escalator_number.STA='0'
            print("ESCALATOR> Modbus error")

    

# #********************* End of Escalator data processing ************************#

