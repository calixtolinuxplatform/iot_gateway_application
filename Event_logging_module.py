#### ****************************************** Import ************************************* ### 

import sys
import time
import pickle
import datetime
import csv
import shutil
import os
import math
import requests
import json
import paramiko
from operator import itemgetter
from collections import deque


#### ****************************************** File inclusion ************************************* ### 
EVT_file_name     = ["./Event_log/Gateway_event_log.csv"]
EVT_max_log_count = [100]
EVT_log_gateway=[]

#*******************************************************************************
  # @brief  file write.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def CSV_file_write (file_name,data_structure,lines):
  with open(file_name, 'w', newline='') as writeFile:
    writer = csv.writer(writeFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for i in range(lines):
        writer.writerow(data_structure[i])
  writeFile.close()

  print("\nStorage Module> File write success: ",file_name)

#******************** End of gateway configuration store ********************#


#*******************************************************************************
  # @brief  file Read.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def CSV_file_read(file_name):
  
  csv.register_dialect('myDialect',delimiter = ',',skipinitialspace=True)
  with open(file_name, 'r') as csvFile:
    reader = csv.reader(csvFile, dialect='myDialect')   
    Gateway_management_data=list(reader)
  csvFile.close()
  return Gateway_management_data

#******************** End of gateway configuration store ********************#

#*******************************************************************************
  # @brief  Event log write.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def EVT_log_write(event_reported):
   now = datetime.datetime.now()
   System_Date = str('{0:02d}'.format(now.day))+str('{0:02d}'.format(now.month))+str('{0:04d}'.format(now.year))
   System_Time = str('{0:02d}'.format(now.hour))+str('{0:02d}'.format(now.minute))+str('{0:02d}'.format(now.second))
   
   EVT_log_gateway.append([event_reported,System_Date,System_Time])

   CSV_file_write (EVT_file_name[0],Gateway_event_log,len(Gateway_event_log))
   print(EVT_log_gateway)
   time.sleep(2)


#******************** End of gateway configuration store ********************#

#*******************************************************************************
  # @brief  Event log write.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def EVT_log_read(file_name):
  print("EVENT> Read function")  

#******************** End of gateway configuration store ********************#

#*******************************************************************************
  # @brief  Event log write.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def EVT_module_init(file_name, max_log_count):
  EVT_file_name[0]     = file_name
  EVT_max_log_count[0] = max_log_count

#******************** End of gateway configuration store ********************#