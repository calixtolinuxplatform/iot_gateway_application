
####**************************************  Import  *****************************************************### 
import datetime
import time
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadInputRegistersResponse
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.compat import iteritems
from common import *


### ****************************************** Constant declaration ************************************* ###



### ****************************************** Variable declaration ************************************* ###
FRZR_register_values =   [0x00,0x00]
frzr_read_interval   =   [10]


### ****************************************** Class declaration ************************************* ###
class freezer_parameters:
    def __init__(self,Manufacturer,Parameter_count):
        self.Manufacturer = Manufacturer
        self.Parameter_count = Parameter_count
    
        self.TMP1  = '-001.0' 
        self.TMP2  = '+001.0'
        self.STA   = '1'
        self.DID   = '03'
        self.DTP   = '03'

    def Get_frzr_parameter_packet(self):
        freezer_parameter_json_packet = "\"DEV\":{\"DID\":\""+self.DID+"\",\"DTP\":\""+self.DTP+"\",\"STA\":\""+self.STA+"\",\"TMP1\":\""+self.TMP1+"\",\"TMP2\":\""+self.TMP2+"\"}}"
        return freezer_parameter_json_packet


freezer1=freezer_parameters("VISA",30)
freezer1.DID = Slave_device_table[1][0]

###***************************************  Function prototype  *************************************************###


# #*******************************************************************************
#   # @brief  Freezer data processing.
#   # @param  Modbu_Slave_ID  - Slave address of the Escalator device
#             Freezer_number  - Freezer class object
#   # @retval None
#   #****************************************************************************#
def Feezer_read_modbus_registers(Modbus_Slave_ID, Freezer_number):
        try:
            client = ModbusClient(method = 'rtu',port = modbus_com,stopbits=2,parity='N',baudrate=19200,timeout=5)
            connection = client.connect()
            print(connection)

            Holding_register_value = client.read_holding_registers(0,2,unit=Modbus_Slave_ID)   # Read Generator running Hours
            client.close()

            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Holding_register_value.registers)

            Freezer_number.TMP1 = s16_to_string(Holding_register_value.registers[0], SIGNED_VALUE, decimal_points=1, string_length=6)
            Freezer_number.TMP2 = s16_to_string(Holding_register_value.registers[1], SIGNED_VALUE, decimal_points=1, string_length=6)
  

            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Freezer_number.TMP1,Freezer_number.TMP2)
            
        except:
            Freezer_number.STA='0'
            print("Freezer> Modbus error")

    

# #********************* End of Freezer data processing ************************#




