
####**************************************  Import  *****************************************************### 
import datetime
import time
import random
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadInputRegistersResponse
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.compat import iteritems
from common import *


### ****************************************** Constant declaration ************************************* ###



### ****************************************** Variable declaration ************************************* ###
GEN_register_values =   [0x01,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04,0x01,0x04,\
                        0x00,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04,\
                        0x00,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04,\
                        0x00,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04,\
                        0x00,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04,\
                        0x00,0x00,0xFF,0xFF,0x00,0x12,0x01,0xFF,0x01,0x04]

gen_read_interval   =   [10]
modbus_com_port     =   ["/dev/ttyO3"]
Generator_running_hours = [1212]

### ****************************************** Class declaration ************************************* ###
class generator_parameters:
    def __init__(self,Manufacturer,Parameter_count):
        self.Manufacturer = Manufacturer
        self.Parameter_count = Parameter_count
    
        self.RHR  = '00080787.55' 
        self.BTV  = '00012'
        self.ESP  = '01000'
        self.ETP  = '+00030'
        self.EOP  = '+0012.3'
        self.FLV  = '+00055'
        self.EOT  = '+00032'
        self.GFQ  = '+0048.5'
        self.VL12 = '+00389'
        self.VL23 = '+00392'
        self.VL31 = '+00390'
        self.VLN1 = '+00238'
        self.CRT1 = '+0030'
        self.PFR1 = '+000.80'
        self.APR1 = '+0750.5'
        self.RPR1 = '+0200.0'
        self.RLP1 = '+0604.0'
        self.VLN2 = '+00238'
        self.CRT2 = '+0030'
        self.PFR2 = '+000.80'
        self.APR2 = '+0750.5'
        self.RPR2 = '+0200.0'
        self.RLP2 = '+0604.0'
        self.VLN3 = '+00238'
        self.CRT3 = '+0030'
        self.PFR3 = '+000.80'
        self.APR3 = '+0750.5'
        self.RPR3 = '+0200.0'
        self.RLP3 = '+0604.0'
        self.EFU  = '+0604.0'
        self.STA  = '0'
        self.DID  = '01'
        self.DTP  = '01'

    def Get_gen_parameter_packet(self):
        generator_parameter_json_packet = "\"DEV\":{\"DID\":\""+self.DID+"\",\"DTP\":\""+self.DTP+"\",\"STA\":\""+self.STA+"\",\"RHR\":\""+self.RHR+"\",\"BTV\":\""+self.BTV+"\",\"ESP\":\""+self.ESP+"\",\"ETP\":\""+self.ETP+"\",\"EOP\":\""+self.EOP+"\",\"FLV\":\""+self.FLV+"\",\"EOT\":\""+self.EOT+"\",\"GFQ\":\""+self.GFQ+"\",\"VL12\":\""+self.VL12+"\",\"VL23\":\""+self.VL23+"\",\"VL31\":\""+self.VL31+"\",\"LN1\":{\"VLN\":\""+self.VLN1+"\",\"CRT\":\""+self.CRT1+"\",\"PFR\":\""+self.PFR1+"\",\"APR\":\""+self.APR1+"\",\"RPR\":\""+self.RPR1+"\",\"RLP\":\""+self.RLP1+"\"},\"LN2\":{\"VLN\":\""+self.VLN2+"\",\"CRT\":\""+self.CRT2+"\",\"PFR\":\""+self.PFR2+"\",\"APR\":\""+self.APR2+"\",\"RPR\":\""+self.RPR2+"\",\"RLP\":\""+self.RLP2+"\"},\"LN3\":{\"VLN\":\""+self.VLN3+"\",\"CRT\":\""+self.CRT3+"\",\"PFR\":\""+self.PFR3+"\",\"APR\":\""+self.APR3+"\",\"RPR\":\""+self.RPR3+"\",\"RLP\":\""+self.RLP3+"\"}}}"
        return generator_parameter_json_packet


generator1=generator_parameters("VISA",30)
generator1.DID = Slave_device_table[1][0]

###***************************************  Function prototype  *************************************************###



#*******************************************************************************
  # @brief  Generator data processing.
  # @param  GEN_parameters - Generator modbus register values
  #         Generator_number - Generator class object
  # @retval None
  #****************************************************************************#

def GEN_data_process(GEN_parameters, Generator_number):

    run_hr_reg = (GEN_parameters[0]<<16|GEN_parameters[1])/100
    #generator1.RHR  = str(float(run_hr_reg/100)).zfill(11)
    Generator_number.RHR  = "%011.2f"%run_hr_reg
    Generator_number.BTV  = s16_to_string(GEN_parameters[2],   UNSIGNED_VALUE, decimal_points=1, string_length=5)
    Generator_number.ESP  = s16_to_string(GEN_parameters[3],   UNSIGNED_VALUE, decimal_points=0, string_length=5)
    Generator_number.ETP  = s16_to_string(GEN_parameters[4],     SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.EOP  = s16_to_string(GEN_parameters[5],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.FLV  = s16_to_string(GEN_parameters[6],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.EOT  = s16_to_string(GEN_parameters[7],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.GFQ  = s16_to_string(GEN_parameters[8],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.VL12 = s16_to_string(GEN_parameters[9],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.VL23 = s16_to_string(GEN_parameters[10],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.VL31 = s16_to_string(GEN_parameters[11],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.VLN1 = s16_to_string(GEN_parameters[12],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.CRT1 = s16_to_string(GEN_parameters[13],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.PFR1 = s16_to_string(GEN_parameters[14],   SIGNED_VALUE, decimal_points=2, string_length=7)
    Generator_number.APR1 = s16_to_string(GEN_parameters[15],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RPR1 = s16_to_string(GEN_parameters[16],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RLP1 = s16_to_string(GEN_parameters[17],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.VLN2 = s16_to_string(GEN_parameters[18],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.CRT2 = s16_to_string(GEN_parameters[19],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.PFR2 = s16_to_string(GEN_parameters[20],   SIGNED_VALUE, decimal_points=2, string_length=7)
    Generator_number.APR2 = s16_to_string(GEN_parameters[21],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RPR2 = s16_to_string(GEN_parameters[22],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RLP2 = s16_to_string(GEN_parameters[23],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.VLN3 = s16_to_string(GEN_parameters[24],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.CRT3 = s16_to_string(GEN_parameters[25],   SIGNED_VALUE, decimal_points=0, string_length=6)
    Generator_number.PFR3 = s16_to_string(GEN_parameters[26],   SIGNED_VALUE, decimal_points=2, string_length=7)
    Generator_number.APR3 = s16_to_string(GEN_parameters[27],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RPR3 = s16_to_string(GEN_parameters[28],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.RLP3 = s16_to_string(GEN_parameters[29],   SIGNED_VALUE, decimal_points=1, string_length=7)
    Generator_number.EFU  = s16_to_string(GEN_parameters[30],   SIGNED_VALUE, decimal_points=0, string_length=6)


#********************* End of generator data processing ************************#

#*******************************************************************************
  # @brief  Generator data grabbing thread.
#   # @param  Modbu_Slave_ID  - Slave address of the Escalator device
#             Generator_number - Generator class object
  # @retval None
  #****************************************************************************#

def Generator_read_modbus_registers(Modbus_Slave_ID, Generator_number):
        try:
            client = ModbusClient(method = 'rtu',port = modbus_com,stopbits=2,parity='N',baudrate=19200,timeout=5)
            connection = client.connect()
            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(connection)
            value_1 = client.read_holding_registers(12,2,unit=Modbus_Slave_ID)   # Read Generator running Hours
            client.close()
            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(value_1.registers)
            GEN_register_values[0]=value_1.registers[0]
            GEN_register_values[1]=value_1.registers[1]
            time.sleep(1)
            client = ModbusClient(method = 'rtu',port = modbus_com,stopbits=2,parity='N',baudrate=19200,timeout=5)
            connection = client.connect()
            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(connection)
            #value = client.read_holding_registers(106,36,unit=0x05)
            value = client.read_holding_registers(106,36,unit=Modbus_Slave_ID)
            client.close()
            generator1.STA='1'
            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(value.registers)

            GEN_register_values[2] = value.registers[0] 
            GEN_register_values[3] = value.registers[6]
            GEN_register_values[4] = value.registers[8] 
            GEN_register_values[5] = value.registers[9] 
            GEN_register_values[6] = value.registers[10] 
            GEN_register_values[7] = value.registers[11] 
            GEN_register_values[8] = value.registers[14] 
            GEN_register_values[9] = value.registers[15] 
            GEN_register_values[10] = value.registers[16] 
            GEN_register_values[11] = value.registers[17]  
            GEN_register_values[12] = value.registers[18] 
            GEN_register_values[13] = value.registers[19]
            GEN_register_values[14] = value.registers[20] 
            GEN_register_values[15] = value.registers[21]
            GEN_register_values[16] = value.registers[22] 
            GEN_register_values[17] = value.registers[23]
            GEN_register_values[18] = value.registers[24] 
            GEN_register_values[19] = value.registers[25]
            GEN_register_values[20] = value.registers[26] 
            GEN_register_values[21] = value.registers[27]
            GEN_register_values[22] = value.registers[28] 
            GEN_register_values[23] = value.registers[29]
            GEN_register_values[24] = value.registers[30] 
            GEN_register_values[25] = value.registers[31]
            GEN_register_values[26] = value.registers[32] 
            GEN_register_values[27] = value.registers[33]
            GEN_register_values[28] = value.registers[34] 
            GEN_register_values[29] = value.registers[35]

            GEN_data_process(GEN_register_values,Generator_number)
        except:
            Generator_number.STA='0'
            print("GEN> Modbus error")

    
        

#************************** End of Generator data processing *******************************#



