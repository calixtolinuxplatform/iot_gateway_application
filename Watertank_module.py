
####**************************************  Import  *****************************************************### 
import datetime
import time
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadInputRegistersResponse
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.compat import iteritems
from common import *


### ****************************************** Constant declaration ************************************* ###



### ****************************************** Variable declaration ************************************* ###
WTRTNK_register_values =   [0x00,0x00,0x00,0x00,0x00]

wtrtnk_read_interval   =   [10]


### ****************************************** Class declaration ************************************* ###
class watertank_parameters:
    def __init__(self,Manufacturer,Parameter_count):
        self.Manufacturer = Manufacturer
        self.Parameter_count = Parameter_count
    
        self.GR1  = '1' 
        self.GR2  = '0'
        self.GR3  = '0'
        self.ORG  = '0'
        self.RED  = '0'
        self.STA  = '1'
        self.DID  = '02'
        self.DTP  = '02'

    def Get_wtrtnk_parameter_packet(self):
        watertank_parameter_json_packet = "\"DEV\":{\"DID\":\""+self.DID+"\",\"DTP\":\""+self.DTP+"\",\"STA\":\""+self.STA+"\",\"GR1\":\""+self.GR1+"\",\"GR2\":\""+self.GR2+"\",\"GR3\":\""+self.GR3+"\",\"ORG\":\""+self.ORG+"\",\"RED\":\""+self.RED+"\"}}"
        return watertank_parameter_json_packet



###***************************************  Function prototype  *************************************************###


watertank1=watertank_parameters("CALIXTO",5)
watertank1.DID = Slave_device_table[1][0]

# #*******************************************************************************
#   # @brief  Watertank data processing.
#   # @param  Modbu_Slave_ID  - Slave address of the Watertank device
#             Watertabk_number - Watertank class object
#   # @retval None
#   #****************************************************************************#
def Watertank_read_modbus_registers(Modbus_Slave_ID, Watertank_number):
        try:
            client = ModbusClient(method = 'rtu',port = modbus_com,stopbits=2,parity='N',baudrate=19200,timeout=5)
            connection = client.connect()
            print(connection)

            Holding_register_value = client.read_holding_registers(0,5,unit=Modbus_Slave_ID)   # Read Generator running Hours
            client.close()

            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Holding_register_value.registers)

            Watertank_number.GR1 = str(Holding_register_value.registers[0])
            Watertank_number.GR2 = str(Holding_register_value.registers[1])
            Watertank_number.GR3 = str(Holding_register_value.registers[2])
            Watertank_number.ORG = str(Holding_register_value.registers[3])
            Watertank_number.RED = str(Holding_register_value.registers[4])
            
            if DEBUG_PRINTS  == DEBUG_ENABLE:
                print(Watertank_number.GR1,Watertank_number.GR2,Watertank_number.GR3,Watertank_number.ORG,Watertank_number.RED)
            
        except:
            Watertank_number.STA='0'
            print("WATERTANK> Modbus error")

    

# #********************* End of Watertank data processing ************************#




