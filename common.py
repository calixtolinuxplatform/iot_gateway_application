####**************************************  Import  *****************************************************### 
import _thread
import sys
from Event_logging_module import *


### ****************************************** Constant declaration ************************************* ###

SLAVE_TYPE_GENERATOR = 1
SLAVE_TYPE_WATERTANK = 2
SLAVE_TYPE_FREEZER   = 3
SLAVE_TYPE_ESCALATOR = 4

DEBUG_ENABLE  = 1
DEBUG_DISABLE = 0
DEBUG_PRINTS  = DEBUG_ENABLE   # Enable debug messages to print in console. For debugging purpose. For production version need to disable this option


#************************* Modbus Setting ***********************#
modbus_com_stop_bits = 2
modbus_com_parity    = 'N'


#*************** Application running Target select ***************#
DEVICE_TYPE_PC      = 1
DEVICE_TYPE_GATEWAY = 2

APPLICATION_TARGET  =  DEVICE_TYPE_PC
#APPLICATION_TARGET  =  DEVICE_TYPE_GATEWAY

if APPLICATION_TARGET  ==  DEVICE_TYPE_PC:
	modbus_com    =   "COM8"
elif APPLICATION_TARGET  ==  DEVICE_TYPE_GATEWAY:
    modbus_com     =   "/dev/ttyO3"


#*******************  Gateway configuration **********************#
Gateway_config_file = CSV_file_read("./Gateway_config.csv")
print(Gateway_config_file)

Gateway_id                   =   Gateway_config_file[1][0]
server_url                   =   Gateway_config_file[1][4]
cloud_push_interval          =   int(Gateway_config_file[1][5])*60
slave_modbus_read_interval   =   int(Gateway_config_file[1][6])
cloud_push_retry             =   int(Gateway_config_file[1][7])
slave_device_count           =   int(Gateway_config_file[1][8])

Maximum_slave_count  = 5
Maximum_log_count    = 100


#******************** Slave configuration ************************#
Slave_device_table   = CSV_file_read("./Slave_device_table.csv")

modbus_slave_address = int(Slave_device_table[1][2])
if DEBUG_PRINTS  == DEBUG_ENABLE:
	if int(Slave_device_table[1][1]) == SLAVE_TYPE_GENERATOR:
		print("Init Module> Slave type Generator")

	elif int(Slave_device_table[1][1]) == SLAVE_TYPE_WATERTANK:
		print("Init Module> Slave type Watertank")

	elif int(Slave_device_table[1][1]) == SLAVE_TYPE_FREEZER:
		print("Init Module> Slave type Freezer")

	elif int(Slave_device_table[1][1]) == SLAVE_TYPE_ESCALATOR:
		print("Init Module> Slave type Escalator")
	else:
		print("Init Module> Invalid slave type")



#*******************************************************************************
  # @brief  Hex to 16 bit signed integer conversion.
  # @param  value  - Inteer Value to be converted
  #         signed_unsigned - sign is required or not
  #         decimal_points - Number of decimal points required
  #         string_length - Length of the string required including decimal point(.) and sign(+/-) 
  # @retval None
  #****************************************************************************#
SIGNED_VALUE   = 0
UNSIGNED_VALUE = 1

def s16_to_string(value, signed_unsigned, decimal_points,string_length):

    if decimal_points > 0:

        if signed_unsigned == 0:    # Signed
            value = -(value & 0x8000) | (value & 0x7fff)
            if value < 0:
                value_string = str(float(value/(10**decimal_points))).zfill(string_length)
            elif value >= 0:
                value_string = '+'+str(float(value/(10**decimal_points))).zfill(string_length-1)
        elif signed_unsigned ==1: # UnSigned
            value_string = str(float(value/(10**decimal_points))).zfill(string_length)
    else:
        if signed_unsigned == 0:    # Signed
            value = -(value & 0x8000) | (value & 0x7fff)
            if value < 0:
                value_string = str(value).zfill(string_length)
            elif value >= 0:
                value_string = '+'+str(value).zfill(string_length-1)
        elif signed_unsigned ==1: # UnSigned
            value_string = str(value).zfill(string_length)

    return value_string

#******************************* End of value to string ************************************#