####**************************************  Import  *****************************************************### 
import _thread
import sys

####  File inclusion  ### 
from Cloud_module import *


### ****************************************** Variable declaration ************************************* ###

###***************************************  Function prototype  *************************************************###


#*******************************************************************************
  # @brief  Read modbus registers.
  # @param  Thread name
  # @retval None
  #****************************************************************************#
def Read_modbus_registers_thread(thread_name):
  while True:
    for i in range(slave_device_count):

      if int(Slave_device_table[i+1][1]) == SLAVE_TYPE_GENERATOR:
        Generator_read_modbus_registers(modbus_slave_address,generator1)

      elif int(Slave_device_table[i+1][1]) == SLAVE_TYPE_WATERTANK:
        Watertank_read_modbus_registers(modbus_slave_address,watertank1)

      elif int(Slave_device_table[i+1][1]) == SLAVE_TYPE_FREEZER:
        Feezer_read_modbus_registers(modbus_slave_address,freezer1)

      elif int(Slave_device_table[i+1][1]) == SLAVE_TYPE_ESCALATOR:
        Esclator_read_modbus_registers(modbus_slave_address,escalator1)

      else:
        print("Invalid slave type")

      time.sleep(1)

    time.sleep(slave_modbus_read_interval)


#******************** End of Read modbus registers thread ********************#




#*******************************************************************************
  # @brief  Application Start.
  #****************************************************************************#

print ("IoT Monitoring FW_2.0 starting....")


try:
   _thread.start_new_thread(Read_modbus_registers_thread,("Read_modbus_registers_thread",))
   _thread.start_new_thread(Cloud_communication_thread,("Cloud_communication_thread",))

except:
   print ("Error unable to start thread")

while 1:
   pass
   

